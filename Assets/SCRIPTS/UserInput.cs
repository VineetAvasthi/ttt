﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UserInput : MonoBehaviour {


	public InputField UserInputSize;
	int CellCount;

	/* 

            It just saves data to which will be used in next scene. 
            Pretty Much simple and self explanatory.
        */
	public void SaveData()

	{
		//CONVERT INPUT INTO INT FORMAT
		CellCount = int.Parse(UserInputSize.text);

		//SAVE 
		PlayerPrefs.SetInt ("CellCount", CellCount);


	}
}
