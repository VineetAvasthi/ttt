﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


/* 

AUTHOR: VINEET AVASTHI
kumarvineetonline@gmail.com
vavasthi@sparana.com

*/

public class GameLogic : MonoBehaviour
{

	Text[] ButtonText;



	private string turn;

	public bool isGameOver;

	public Text TurnNotifier, ScoreX, ScoreO;

	Button ActiveButton;

	public CustomTiles customTileScript;

	public string nth_Tile;



	string LEFT, LEFT_2, RIGHT, RIGHT_2, UP, UP_2, DOWN, DOWN_2;

	int GRID_SIZE, q, ROW, COL;

	// VARIABLES FOR EFFECTS
	public AudioSource ButtonClick, WinSound;

	public GameObject BG_Panel;


	Animator WinEffectAnimation;

	void Awake ()
	{

		GRID_SIZE = PlayerPrefs.GetInt ("CellCount");


		//As directed in assignment. First move should be by X.
		turn = "X";

		//To track Game Status and update notification
		isGameOver = false;

		WinEffectAnimation = BG_Panel.GetComponent<Animator> ();
	}



	public string Getturn ()
	{
		return turn;
	}

	public void DetectPattern ()
	{


		ButtonClick.Play ();
		//WinEffectAnimation.Play ("DefaultState");
		//1. GET TILE ADDRES
		q = int.Parse (nth_Tile);


		Debug.Log ("COLS, ROWS   ::" + q % GRID_SIZE + "  ,  " + q / GRID_SIZE);


		//2. COMPARE NEIGHBORING TILES  FOR WIN


		COL = q % GRID_SIZE;
		ROW = q / GRID_SIZE;

		//HORIZONTAL VALIDATION
		H_VALIDATION();


		//VERTICAL VALIDATION
		V_VALIDATION ();


		//DIAGONAL VALIDATION
		D_VALIDATION ();



		SwitchPlayer ();


		/*Debug.Log ("Q" + q);

		Debug.Log ("GRID" + GRID_SIZE);

		Debug.Log ("N/A" + (q - GRID_SIZE));

		Debug.Log ("COL " + COL);

		Debug.Log ("ROw " + ROW);

		Debug.Log ("COL_1 " + (COL + 1));

		Debug.Log ("ROw_1 " + (ROW + 1)); */
		WinEffectAnimation.Play ("DefaultState");
	}

		




	void SwitchPlayer ()
	{
		turn = (turn == "X") ? "O" : "X";

		if (isGameOver == false) {
			

		}

	}

	void GameOver ()
	{
		isGameOver = true;

		/*for (int i = 0; i < ButtonText.Length; i++)
			
		{
			ButtonText[i].GetComponentInParent<Button>().interactable = false;


		}*/


		Debug.Log ("Game Won By: " + turn);


	
	
	}


	int SCORE_X, SCORE_O = 0;

	public void WIN ()
	{


		StartCoroutine (BG_Effect (2.0f));
		WinSound.Play ();

		if (turn == "X") {
		
			SCORE_X += 1;

			ScoreX.text = "X:" + SCORE_X.ToString ();
		
		}

		if (turn == "O") {

			SCORE_O += 1;

			ScoreO.text = "O:" + SCORE_O.ToString ();

		}



		Debug.Log ("WINNER WINNER WINNER WINNER WINNER WINNER WINNER WINNER WINNER WINNER WINNER WINNER WINNER WINNER WINNER ");


	}


	IEnumerator BG_Effect (float t)
	{
	
		WinEffectAnimation.Play ("WinAnimation");

		yield return new WaitForSeconds (t);


		WinEffectAnimation.Play ("DEFAULT");

	}


	void H_VALIDATION ()
	{


		if (COL <= GRID_SIZE - 2) {
			Debug.Log ("HORIZONTAL LESS");
		
			if (customTileScript.ButtonList [(q + 1)].GetComponentInChildren<Text> ().text == turn) {



				if (customTileScript.ButtonList [(q + 2)].GetComponentInChildren<Text> ().text == turn) {



					WIN ();

					DisableUsedButtons (q);
					DisableUsedButtons (q + 1);
					DisableUsedButtons (q + 2);

				}

				if (COL > 0) {
					if (customTileScript.ButtonList [(q - 1)].GetComponentInChildren<Text> ().text == turn) {


						DisableUsedButtons (q);
						DisableUsedButtons (q + 1);
						DisableUsedButtons (q - 1);
					}
				}


			}

		}

		if (COL >= 2) {



			if (customTileScript.ButtonList [(q - 1)].GetComponentInChildren<Text> ().text == turn) {


				if (customTileScript.ButtonList [(q - 2)].GetComponentInChildren<Text> ().text == turn) {


					WIN ();
					DisableUsedButtons (q);
					DisableUsedButtons (q - 2);
					DisableUsedButtons (q - 1);

				}


				if (customTileScript.ButtonList [(q + 1)].GetComponentInChildren<Text> ().text == turn) {


					WIN ();
					DisableUsedButtons (q);
					DisableUsedButtons (q + 1);
					DisableUsedButtons (q - 1);
				}

				if (customTileScript.ButtonList [(q + 2)].GetComponentInChildren<Text> ().text == turn) {


					WIN ();
					DisableUsedButtons (q);
					DisableUsedButtons (q + 2);
					DisableUsedButtons (q - 1);
				}
			}
	
		}
	}


	void V_VALIDATION ()
	{
		if (ROW <= GRID_SIZE - 2) {

			if (customTileScript.ButtonList [(q + GRID_SIZE)].GetComponentInChildren<Text> ().text == turn) {


				if (ROW < GRID_SIZE - 2) {


					if (customTileScript.ButtonList [q + (GRID_SIZE * 2)].GetComponentInChildren<Text> ().text == turn) {


						WIN ();
						DisableUsedButtons (q);
						DisableUsedButtons (q + GRID_SIZE);
						DisableUsedButtons (q + (GRID_SIZE * 2));

					}


				} else {
				
				
					if (customTileScript.ButtonList [(q - GRID_SIZE)].GetComponentInChildren<Text> ().text == turn) {


						WIN ();
						DisableUsedButtons (q);
						DisableUsedButtons (q + GRID_SIZE);
						DisableUsedButtons ((q - GRID_SIZE));

					}
				
				
				}

		
		
			}
		}



		if (ROW >= 2) {
		

			if (customTileScript.ButtonList [q - GRID_SIZE].GetComponentInChildren<Text> ().text == turn) {
				Debug.Log ("2");

				if (customTileScript.ButtonList [q - (GRID_SIZE * 2)].GetComponentInChildren<Text> ().text == turn) {


					WIN ();
					DisableUsedButtons (q);
					DisableUsedButtons (q - GRID_SIZE);
					DisableUsedButtons (q - (GRID_SIZE * 2));

				}


				if (ROW < GRID_SIZE - 2) {


					if (customTileScript.ButtonList [(q + GRID_SIZE)].GetComponentInChildren<Text> ().text == turn) {


						if (customTileScript.ButtonList [(q + GRID_SIZE) * 2].GetComponentInChildren<Text> ().text == turn) {


							WIN ();
							DisableUsedButtons (q);
							DisableUsedButtons (q - GRID_SIZE);
							DisableUsedButtons ((q - GRID_SIZE) * 2);

						}

					}
			



				}
			}
		
		
		}

	}

	void D_VALIDATION ()
	{

	
		if ((ROW <= GRID_SIZE - 2) && (COL <= GRID_SIZE - 2)) {
			Debug.Log ("VERTICAL LESS");
			if (customTileScript.ButtonList [(q + (GRID_SIZE + 1))].GetComponentInChildren<Text> ().text == turn) {

				if ((q + (GRID_SIZE * 2 + 2)) <= (GRID_SIZE * GRID_SIZE)) {
					if (customTileScript.ButtonList [(q + (GRID_SIZE * 2 + 2))].GetComponentInChildren<Text> ().text == turn) {
						Debug.Log ("DIAG Down matches");
						WIN ();

                    				DisableUsedButtons (q);
						DisableUsedButtons ((q + (GRID_SIZE + 1)));
						DisableUsedButtons ((q + (GRID_SIZE * 2 + 2)));
           

					}
				}  if ((q - (GRID_SIZE + 1)) >= 0) {


						if (customTileScript.ButtonList [(q - (GRID_SIZE + 1))].GetComponentInChildren<Text> ().text == turn) {
							Debug.Log ("Click Mid - Diag match");
							WIN ();
							
						DisableUsedButtons (q);
						DisableUsedButtons ((q + (GRID_SIZE + 1)));
						DisableUsedButtons ((q - (GRID_SIZE + 1)));
                        
						}
					}
			
				
			}





		}
		if (COL >= 1) {

			Debug.Log ("VERTICAL LESS XXXX");
			if ((q + (GRID_SIZE - 1)) < (GRID_SIZE * GRID_SIZE))
			if (customTileScript.ButtonList [(q + (GRID_SIZE - 1))].GetComponentInChildren<Text> ().text == turn) {

			
				if ((q + (GRID_SIZE * 2 - 2)) <= (GRID_SIZE * GRID_SIZE)) {
					if (customTileScript.ButtonList [(q + (GRID_SIZE * 2 - 2))].GetComponentInChildren<Text> ().text == turn) {

						WIN ();
						DisableUsedButtons (q);
						DisableUsedButtons ((q + (GRID_SIZE - 1)));
						DisableUsedButtons ((q + (GRID_SIZE * 2 - 2)));

					} else if ((COL > 0) && (ROW > 0)) {
						if (customTileScript.ButtonList [(q - (GRID_SIZE - 1))].GetComponentInChildren<Text> ().text == turn) {
							Debug.Log ("Click Mid - Diag match XXXX");
							WIN ();
							DisableUsedButtons (q);
							DisableUsedButtons ((q + (GRID_SIZE - 1)));
							DisableUsedButtons ((q - (GRID_SIZE - 1)));
						}
					}
				}
			}
			Debug.Log ("Diagonal bottom click check>>>>>>>");

			if ((q - (GRID_SIZE + 1)) >= 0)
			if (customTileScript.ButtonList [(q - (GRID_SIZE + 1))].GetComponentInChildren<Text> ().text == turn) {

				Debug.Log ("LOS POLLOS");
				if ((q - (GRID_SIZE * 2 + 2)) >= 0) {

					Debug.Log ("HARMANOS");

					if (customTileScript.ButtonList [(q - (GRID_SIZE + 1) * 2)].GetComponentInChildren<Text> ().text == turn) {
						WIN ();

						DisableUsedButtons (q);
						DisableUsedButtons ((q - (GRID_SIZE + 1)));
						DisableUsedButtons ((q - (GRID_SIZE + 1) * 2));

					}


				}
			}
		}
		if (ROW >= 2) {
			Debug.Log ("Diagonal bottom click check");
			if ((q - (GRID_SIZE - 1)) >= 0) {

				Debug.Log ("BREAKING BAD");
				if (customTileScript.ButtonList [(q - (GRID_SIZE - 1))].GetComponentInChildren<Text> ().text == turn) {

					if ((q - (GRID_SIZE * 2 - 2)) >= 0) {

						if (customTileScript.ButtonList [(q - (GRID_SIZE * 2 - 2))].GetComponentInChildren<Text> ().text == turn) {
							WIN ();
				

							DisableUsedButtons (q);
							DisableUsedButtons ((q - (GRID_SIZE - 1)));
							DisableUsedButtons ((q - (GRID_SIZE * 2 - 2)));


						}
					}
				}
			}
		}


	}

	void DisableUsedButtons (int i)
	{
	
		customTileScript.ButtonList [(i)].GetComponentInChildren<Text> ().text = "";

	}
}



 
