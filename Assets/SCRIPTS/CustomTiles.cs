﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

/* 

AUTHOR: VINEET AVASTHI
kumarvineetonline@gmail.com
vavasthi@sparana.com

SUMMARY:

To save space and for the sake of optimization, 
A single button PREFAB is OBJECT POOLED with --ObjectPool-- Function.
At runtime it allocates space saving the cost of size in binary generated file.

Then --DynamicGrid-- Function adjusts any number of cells/buttons entered by user on display.

These functions are called in --Start-- Function.

*/

public class CustomTiles : MonoBehaviour {


	//BUTTON PREFAB
	public Button button;

	//BUTTON COUNT
	 static int CellNo;

	//PARENT UNDER WHICH THE OBJEC POOLED BUTTONS LIVE
	public GameObject parentGB;

	//
	public RectTransform ParentContainer;
	//public GridLayoutGroup grid;

	public List<Button> ButtonList;

	//Text[] z;

	public int[,] GRID;

	// Use this for initialization
	void Start () 

	{
		ButtonList = new List<Button> ();
	

		CellNo = PlayerPrefs.GetInt ("CellCount");

		///z = new Text[CellNo];

	
		GRID = new int[CellNo, CellNo ];

		ObjectPool ( button, CellNo*CellNo );

		DynamicGrid (CellNo);



	}
	

	//OBJECT POOL FUNCTION  
	public void ObjectPool (Button button, int number )

	{

		for ( int i = 0; i < number; i++ ) 
		
		{

			Button anyButton = Instantiate (button);
			Text ButtonText = anyButton.GetComponentInChildren<Text> ();

			anyButton.transform.SetParent (parentGB.transform);
			anyButton.name = i.ToString();

			ButtonList.Add (anyButton);
			//ButtonText.text = i.ToString ();
				
		}

	}


	public void DynamicGrid( int number)

	{



		//ParentContainer      = gameObject.GetComponent<RectTransform> ();
		GridLayoutGroup grid  = ParentContainer.GetComponent<GridLayoutGroup> ();

		grid.cellSize = new Vector2 ( 130.0f, 130.0f);
		grid.constraintCount = number;
		
			 
		}







}