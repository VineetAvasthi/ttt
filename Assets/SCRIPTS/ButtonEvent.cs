﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonEvent : MonoBehaviour {

/* 

AUTHOR: VINEET AVASTHI
kumarvineetonline@gmail.com
vavasthi@sparana.com

*/

	public Button ButtonCell;
	public Text ButtonText;

	public Color disabledColor;

	 GameLogic gameLogicScript;
	// Use this for initialization
	void Start () 

	{
		gameLogicScript = Camera.main.GetComponent<GameLogic> ();


	}


	public void GameOn( Button btn )


	{

		//Change Button text
		ButtonText.text = gameLogicScript.Getturn();

		//Disable Clicked Button;
		ButtonCell.interactable = false;

		ButtonCell.GetComponent<Image> ().color = disabledColor;

		gameLogicScript.nth_Tile = btn.name;

		//Detect if there's a winning pattern with every move of either player.
		gameLogicScript.DetectPattern ( );

		gameLogicScript.TurnNotifier.text = "TURN: "+gameLogicScript.Getturn ();

		//Debug.Log ("Tile Adress: "+ btn.name);


	}


	public void Changetext()

	{



	}
}
